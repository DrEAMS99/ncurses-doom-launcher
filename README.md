# ncurses-doom-launcher or cdoom

## Introduction
This project aims to create a simple doom launcher that is used within a terminal emulator. It's definitly not perfect but is perfectly usable for now. This is a project for me to improve my programming skills so things here will probably not be best practice or perfect. If you find it usefull awesome.

## Features
    * Scrolling
    * Set options per game including fast monsters, difficulty, pistol start,...
    * use of wasd, arrow keys, or vim keys to navigate
    * a wizard to add new games easier than trying to manually
    * support for MOST source ports
    * supports loading .pk3s and .pwads for mods

## To Do

    * find a way to support saving the last map that was played or the last save made, or both.
