#include <vector>
#include <wordexp.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string.h>
#include <fstream>
#include <ncurses.h>
#include <ctype.h>

using namespace std;


struct WAD
{
    string name;
    string pistol;
    string complevel;
    string fast;
    string shorttics;
    string pwads;
    string iwad;
    string port;
    string map;
}wad;

void updateInfoWin(WINDOW *infowin, WAD game, int yMax, int xMax);
int checkSafePort(string port);

int main()
{

    //this is for cycling ports
    unsigned char port_number=0;

    int numberWads=0;
    bool record= false;

    /*Read File for Amount of Lines and the content of each*/
    //input_stream is the file that holds configurations for each "game"
    fstream input_stream;

    //Getting the location of the config file
    string configFile=getenv("HOME");
    configFile=configFile+"/.config/cdoom/cdoom.rc";

    //Counting the amount of lines
    input_stream.open(configFile, ios::in);

    //Checks if configFile exists if it doesn't
    //tell user to make it and quit
    if (!input_stream)
    {
        printf("Make the file ~/.config/cdoom/cdoom.rc either manualy or with our script\n");
        return 0;
    }

    //If file exists and check for corrrct number of commas in config
    if (input_stream.is_open())
    {
        string line;
        int lineNum=1;
        while (!input_stream.eof())
        {
            getline(input_stream,line);
            numberWads++;



            int commas=0;
            for (int i=0;i<line.size();i++)
            {
                if (line[i] == ',')
                    commas++;
            }

            //number is actually one more than actuall commas will figure out why
            if ((commas != 8 || !line.size() ) && input_stream.peek()!=EOF)
            {
                printf("Your configurarion exists in such a way that breaks ");
                printf("the program.\nThe problem seems to persist on line ");
                printf("%d\n\n", lineNum);
                printf("%s\n", line.c_str());
                return 0;
            }
            lineNum++;
        }
        //This allows for the EXIT Choice

    }

    input_stream.close();

    //Reading content into array of structs
    if (numberWads == 0)
        numberWads=1;

    WAD wads[numberWads];

    input_stream.open(configFile,ios::in);

    if (input_stream.is_open())
    {
        string line, temp;
        vector<string> cat;
        int i=0;
        int j=0;

        wads[0].name = "";
        wads[0].pistol = "";
        wads[0].complevel = "";
        wads[0].fast = "";
        wads[0].shorttics = "";
        wads[0].pwads = "";
        wads[0].iwad = "";
        wads[0].map = "";
        wads[0].port = "";

        while (! input_stream.eof() )
        {
            getline (input_stream,line);


            stringstream lineStream(line);


            string cell;

            while(getline(lineStream,cell,','))
            {
                cat.push_back(cell);
            }

            if (!lineStream && cell.empty())
            {
                cat.push_back("");
            }

            //If you add a parameter at all example "shorttics" all numbers
            //need to be added by after shorttics pwads went from 4+j to 5+j
            //also applys to j = j+x in which j+7 turned to j+8
            if (i != 0)
            {
                wads[i].name = cat[0+j];
                wads[i].pistol = cat[1+j];
                wads[i].complevel = cat[2+j];
                wads[i].fast = cat[3+j];
                wads[i].shorttics = cat[4+j];
                wads[i].pwads = cat[5+j];
                wads[i].iwad = cat[6+j];
                wads[i].map = cat[7+j];
                wads[i++].port= cat[8+j];
                j = j+9;
            }
            else
                i++;


        }

    }

    input_stream.close();




    /* Initilize Ncurses*/
    initscr();
    noecho();
    cbreak();

    //c is character and highlight is the highlighted choice
    //wads is an array of wads
    int c;
    int highlight=1;

    //Getting Window Size
    int xMax,yMax;
    getmaxyx(stdscr,yMax,xMax);

    //Initalize and make MenuWindow
    //Hieght,Width,starty, startx)
    WINDOW * menuwin = newwin(yMax-1,(xMax/2)-10,0,0);
    box(menuwin,0,0);
    refresh();
    wrefresh(menuwin);

    //Initalize and make InfoWindow
    //Hieght,Width,starty, startx)
    WINDOW * infowin = newwin(yMax-1,(xMax/2)+10,0,(xMax/2)-10);
    box(infowin,0,0);
    refresh();
    wrefresh(infowin);

    //Initilize and make SearchWindow
    //Hieght,Width,starty, startx)
    WINDOW * searchwin = newwin(1,(xMax/2),yMax-1,0);
    wprintw(searchwin,":");
    wrefresh(searchwin);

    //Keypad initalization
    keypad(menuwin, true);
    string choices[numberWads];
    choices[0]="EXIT";
    for (int i=1; i<numberWads;i++)
        choices[i]=wads[i].name;


    //This is for the scroll offest if the amount of selections is bigger than
    //the size of the window/screen
    int scrolloffset = 0;
    int scrollmax = (numberWads-yMax)+3;
    updateInfoWin(infowin,wads[1],yMax,xMax);

    //If user wants to record this is where the path is stored
    char recordPath[75];
    char mapNumber[10];

    //For searching for game
    char searchKey[30];
    char lastsearch[30] = { 0 };

    while(1)
    {
        //creating our menu window
        wclear(menuwin);
        box(menuwin,0,0);
        refresh();

        updateInfoWin(infowin,wads[highlight],yMax,xMax);

        //This is spacing for the text of the options in our menu
        //this is so things dont write on top of one another

        int spacing = 0;

        //This is for scrolling. if the terminal screen is not
        //big enough to hold all options this makes it so it scrolls
        if (numberWads > yMax-4)
        {
            if(highlight > (yMax - 4 + scrolloffset) && scrolloffset < scrollmax)
                scrolloffset++;
            if(highlight < scrolloffset && scrolloffset != 0)
            scrolloffset--;
        }
        //This prints out the menu
        for (int i = scrolloffset; i < (yMax-3)+scrolloffset && i < numberWads; i++)
        {
            //Highlights the selected option
            if(i == highlight)
            {
                wattron(menuwin,A_REVERSE);
            }


            //Printing to terminal
            mvwprintw(menuwin,spacing+1,1,choices[i].c_str());
            wattroff(menuwin, A_REVERSE);
            spacing++;

        }

        //our keyboard input
        c = wgetch(menuwin);

        switch(c)
        {
            //Supports wasd, arrow keys, and vim keys
            case KEY_UP: case 'k': case 'w':
                highlight--;
                if (highlight == -1)
                {
                    highlight=numberWads-1;
                    if (numberWads > yMax - 4)
                        scrolloffset=scrollmax;
                }
                updateInfoWin(infowin,wads[highlight],yMax,xMax);
                break;
            case KEY_DOWN: case 'j': case 's':
                highlight++;
                if (highlight == numberWads)
                {
                    highlight = 0;
                    scrolloffset = 0;
                }
                updateInfoWin(infowin,wads[highlight],yMax,xMax);
                break;
            case 'r':
                record = true;
                break;
            default:
                break;

        }

        //This is for recording demos
        //'r' == r
        if (c == 'r')
        {
            record = true;

            //This makes it so we really delete the windows
            wborder(infowin, ' ', ' ', ' ',' ',' ',' ',' ',' ');
            wborder(menuwin, ' ', ' ', ' ',' ',' ',' ',' ',' ');
            wclear(infowin);
            wclear(menuwin);
            wrefresh(infowin);
            wrefresh(menuwin);

            //really deleting windows
            delwin(infowin);
            delwin(menuwin);

            //Creating window for record path
            WINDOW * recordwin = newwin(yMax-1,xMax,0,0);
            box(recordwin,0,0);
            wrefresh(recordwin);

            getmaxyx(recordwin,yMax,xMax);

            //Get path for recording
            mvprintw(yMax/2,(xMax/2)-20,"./ or nothing means this directory");
            mvprintw((yMax/2)+1,(xMax/2)-20,"Empty will make no recording");
            mvprintw((yMax/2)+2,(xMax/2)-20,"Enter the path for your recording: ");

            //Getting text from user
            echo();
            mvgetnstr((yMax/2)+2,(xMax/2)-20+34,recordPath,70);
            wrefresh(recordwin);


            if (recordPath[1] == '\0')
            {
                record = false;
                break;
            }

            wclear(recordwin);
            wrefresh(recordwin);

            //Get map number
            mvprintw(yMax/2,(xMax/2)-20,"DOOM1: E# M# ANYTHING ELSE: MAP#");
            mvprintw((yMax/2)+1,(xMax/2)-20,"Blank will stop recording");
            mvprintw((yMax/2)+2,(xMax/2)-20,"Enter the map number: ");

            mvgetnstr((yMax/2)+2,(xMax/2)-20+34,mapNumber,3);
            wrefresh(recordwin);

            if (mapNumber[1] == '\0')
            {
                record = false;
                break;
            }


            break;
        }

        //This is used to cycle through ports
        if(c == 'p')
        {

            switch (port_number++) {

                case 0:
                        if    (wads[highlight].port != "dsda-doom" )
                        {
                            wads[highlight].port = "dsda-doom";
                            break;
                        }
                case 1:
                        if    (wads[highlight].port != "woof" )
                        {
                            wads[highlight].port = "woof";
                            break;
                        }
                case 2:
                        if  (wads[highlight].port != "crispy-doom" )
                        {
                            wads[highlight].port = "crispy-doom";
                            break;
                        }
                case 3:
                        if  (wads[highlight].port != "choclate-doom" )
                        {
                            wads[highlight].port = "choclate-doom";
                            break;
                        }
                case 4:
                        if  (wads[highlight].port != "gzdoom" )
                        {
                            wads[highlight].port = "gzdoom";
                            break;
                        }
                case 5:
                        if  (wads[highlight].port != "prboom-plus" )
                        {
                            wads[highlight].port = "prboom-plus";
                            break;
                        }
                case 6:
                        if  ( wads[highlight].port != "nugget" )
                        {
                            wads[highlight].port = "nugget";
                            break;
                        }
                case 7:
                        if  (wads[highlight].port != "eternity" )
                        {
                            wads[highlight].port = "eternity";
                            break;
                        }
                case 8:
                        if  (wads[highlight].port != "zandronum")

                        {
                            wads[highlight].port = "zandronum";
                            port_number=0;
                            break;
                        }
                default:
                        if  (wads[highlight].port != "broken")
                        {
                            wads[highlight].port = "broken";
                            break;
                        }

            }

        }

        // / for searching
        if (c == '/')
        {
            echo();
            wrefresh(searchwin);
            mvgetnstr(yMax-1,1,searchKey,30);

            noecho();
            wclear(searchwin);
            wprintw(searchwin,":");
            wrefresh(searchwin);

            //Converting search to lowercase to get case insensitive searching
            size_t len = strlen(searchKey);
            char *lower = (char*)calloc(len+1, sizeof(char));

            //For our game names
            for (size_t c = 0; c < len; c++)
            {
                lower[c] = tolower((unsigned char)searchKey[c]);
            }

            //compare to each game name
            for (int n = highlight+1; n < numberWads; n++)
            {

                //Make characters all lowercase for easy comparison
                size_t name_len = strlen(wads[n].name.c_str());
                char *name_lower = (char*)calloc(name_len+1, sizeof(char));
                for (size_t c = 0; c < len; c++)
                {
                    name_lower[c] = tolower((unsigned char)wads[n].name.c_str()[c]);
                }

                //This is the comparason
                if ( strstr(name_lower,lower) != NULL)
                {
                   highlight = n;
                   memcpy(lastsearch, name_lower, sizeof(&name_lower));
                   break;
                }

            }

        }
        //For continuing search
        if(c == 'n' && lastsearch[1] != 0)
        {
            size_t len = strlen(lastsearch);

            for (int n = highlight+1; n < numberWads; n++)
            {

                //Make characters all lowercase for easy comparison
                size_t name_len = strlen(wads[n].name.c_str());
                char *name_lower = (char*)calloc(name_len+1, sizeof(char));
                for (size_t c = 0; c < len; c++)
                {
                    name_lower[c] = tolower((unsigned char)wads[n].name.c_str()[c]);
                }

                //This is the comparason
                if ( strstr(name_lower,lastsearch) != NULL)
                {
                   highlight = n;
                   break;
                }

            }

        }
        //10 == Enter
        if(c == 10)
            break;
        //27 == ESC
        if(c == 27)
        {
            printf ("You Have Exited\n");
            return 0;
        }
    }


    //Stop ncurses
    endwin();

    //Check if Quit was chosen otherwise start game
    if ( wads[highlight].name != "")
    {

        string command = wads[highlight].port + " -iwad " + wads[highlight].iwad + " -file " + wads[highlight].pwads;


        if (record)
        {
            command = command + " -record " + recordPath + " -warp " + mapNumber;
        }

        if ( wads[highlight].complevel != "" )
            command = command + " -complevel " + wads[highlight].complevel;

        if ( wads[highlight].fast == "YES" )
            command = command + " -fast ";

        if ( ( wads[highlight].pistol == "YES" ) && !(record) )
            command = command + " -pistolstart ";

        if ( wads[highlight].shorttics == "YES" )
            command = command + " -shorttics ";

        system(command.c_str());

        //Add a way to save last map played



    }

    return 0;
}

void updateInfoWin(WINDOW *infowin, WAD game, int yMax, int xMax)
{

    wclear(infowin);
    box(infowin,0,0);

    mvwprintw(infowin,2,2,"Name: %s",game.name.c_str());
    mvwprintw(infowin,4,2,"Pistol Start: %s",game.pistol.c_str());
    mvwprintw(infowin,6,2,"Short Tics: %s", game.shorttics.c_str());
    mvwprintw(infowin,8,2,"Comp Level: %s",game.complevel.c_str());
    mvwprintw(infowin,10,2,"Fast Monsters: %s",game.fast.c_str());
    mvwprintw(infowin,12,2,"PWADS: %s",game.pwads.c_str());
    mvwprintw(infowin,14,2,"IWADS: %s",game.iwad.c_str());
    mvwprintw(infowin,16,2,"Map: %s",game.map.c_str());
    mvwprintw(infowin,18,2,"Port: %s",game.port.c_str());

    wrefresh(infowin);

}
